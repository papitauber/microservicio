package com.grupoWeb.service;

import java.util.List;

import com.grupoWeb.model.entity.Item;
import com.grupoWeb.model.entity.Sale;
import com.grupoWeb.model.entity.Shipping;

public interface SaleService extends CRUDService<Sale>{

	List<Sale> getAllByShipping(Shipping shipping);
	
	//List<Item> getAllBySale(Sale sale);

}
