package com.grupoWeb.service;

import java.util.List;

import com.grupoWeb.model.entity.Item;
import com.grupoWeb.model.entity.Product;
import com.grupoWeb.model.entity.Sale;

public interface ItemService extends CRUDService<Item> {
	List<Item> getAllByProduct(Product product);
	Item getPriceByProduct(Product product);
	List<Item> getAllBySale(Long idSale);
}
