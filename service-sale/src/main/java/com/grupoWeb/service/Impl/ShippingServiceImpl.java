package com.grupoWeb.service.Impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grupoWeb.model.entity.Shipping;
import com.grupoWeb.model.repository.ShippingRepository;
import com.grupoWeb.service.ShippingService;

@Service
public class ShippingServiceImpl implements ShippingService {

	@Autowired
	private ShippingRepository shippingRepository;
	
	@Transactional
	@Override
	public Shipping insertOrUpdate(Shipping entity) {
		// TODO Auto-generated method stub
		return shippingRepository.save(entity);
	}

	@Override
	public Optional<Shipping> getOne(Long id) {
		// TODO Auto-generated method stub
		return shippingRepository.findById(id);
	}

	@Override
	public List<Shipping> getAll() {
		// TODO Auto-generated method stub
		return shippingRepository.findAll();
	}

	@Transactional
	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		shippingRepository.deleteById(id);
	}



}
