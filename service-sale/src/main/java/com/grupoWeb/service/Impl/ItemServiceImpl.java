package com.grupoWeb.service.Impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grupoWeb.model.entity.Item;
import com.grupoWeb.model.entity.Product;
import com.grupoWeb.model.entity.Sale;
import com.grupoWeb.model.repository.ItemRepository;
import com.grupoWeb.model.repository.SaleRepository;
import com.grupoWeb.service.ItemService;
import com.grupoWeb.service.SaleService;

@Service
public class ItemServiceImpl implements ItemService {
	@Autowired
	private ItemRepository itemRepository;
	
	@Autowired
	private SaleService saleService;
	@Autowired
	private SaleRepository saleRepository;
	
	@Transactional
	@Override
	public Item insertOrUpdate(Item entity) {
		// TODO Auto-generated method stub
		entity.setTotal(calcularTotal(entity));
		Long id = idSaleItem(entity);
		Sale sale = this.saleRepository.getOne(id);
		itemRepository.save(entity);
		this.saleService.insertOrUpdate(sale);
		return null;
	}

	@Override
	public Optional<Item> getOne(Long id) {
		// TODO Auto-generated method stub
		return itemRepository.findById(id);
	}

	@Override
	public List<Item> getAll() {
		// TODO Auto-generated method stub
		List<Item> items = new ArrayList<>();
		itemRepository.findAll().iterator().forEachRemaining(items::add);
		return items;
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		itemRepository.deleteById(id);
	}


	@Override
	public List<Item> getAllByProduct(Product product) {
		// TODO Auto-generated method stub
		return itemRepository.findAllByProduct(product);
	}

	@Override
	public Item getPriceByProduct(Product product) {
		// TODO Auto-generated method stub
		return itemRepository.findPriceByProduct(product);
	}
	private double calcularTotal(Item item) {
		return item.getQuantity()*item.getProduct().getPrice();
	}

	@Override
	public List<Item> getAllBySale(Long idSale) {
		// TODO Auto-generated method stub
		return itemRepository.findAllBySale(idSale);
	}
	
	private Long idSaleItem(Item entity) {
		return entity.getSale().getIdSale();
	}
}
