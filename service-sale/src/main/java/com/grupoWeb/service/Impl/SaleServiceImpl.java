package com.grupoWeb.service.Impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grupoWeb.model.entity.Item;
import com.grupoWeb.model.entity.Sale;
import com.grupoWeb.model.entity.Shipping;
import com.grupoWeb.model.repository.ItemRepository;
import com.grupoWeb.model.repository.SaleRepository;
import com.grupoWeb.service.ItemService;
import com.grupoWeb.service.SaleService;

@Service
public class SaleServiceImpl implements SaleService {

	@Autowired
	private SaleRepository saleRepository;
	
	@Autowired
	private ItemService itemService;
	
	@Transactional
	@Override
	public Sale insertOrUpdate(Sale entity) {
		// TODO Auto-generated method stub
		entity.setTotal(calcularTotal(entity));
		entity.setIgv(calcularIgv(calcularTotal(entity)));
		return saleRepository.save(entity);
	}

	@Override
	public Optional<Sale> getOne(Long id) {
		// TODO Auto-generated method stub
		return saleRepository.findById(id);
	}

	@Override
	public List<Sale> getAll() {
		// TODO Auto-generated method stub
		List<Sale> sales = new ArrayList<>();
		saleRepository.findAll().iterator().forEachRemaining(sales::add);
		return sales;
	}

	@Transactional
	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		saleRepository.deleteById(id);
	}

	@Override
	public List<Sale> getAllByShipping(Shipping shipping) {
		// TODO Auto-generated method stub
		return saleRepository.findAllByShipping(shipping);
	}

	private double calcularTotal(Sale sale) {
		double total = 0.0;
		List<Item> items = this.itemService.getAllBySale(sale.getIdSale());
		for(int i=0;i<items.size();i++) {
			total=total+items.get(i).getTotal();
		}
		return sale.getShipping().getPriceShipping()+total;
	}

	private double calcularIgv(double total) {
		return 0.18*total;
	}



}
