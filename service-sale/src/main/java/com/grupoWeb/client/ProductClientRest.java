package com.grupoWeb.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.grupoWeb.model.entity.Product;

@FeignClient(name="service-product-catalog")
public interface ProductClientRest {
	@GetMapping("/product")
	public List<Product> getProducts();
	@GetMapping("/product/{id}")
	public Product getProductById(@PathVariable Long id);
}
