package com.grupoWeb.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;
import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.grupoWeb.Mapper;
import com.grupoWeb.model.entity.Item;
import com.grupoWeb.model.entity.Product;
import com.grupoWeb.service.ItemService;
import com.grupoWeb.viewmodel.ItemViewModel;

@RestController
@RequestMapping("/item")
public class ItemController {

	@Autowired
	private ItemService itemService;
		
	@Autowired
	private Mapper mapper;

	@GetMapping
	public ResponseEntity<List<ItemViewModel>> all() {
		// map from entity to view model
		List<ItemViewModel> itemViewModel =this.itemService.getAll().stream().map(item -> this.mapper.convertToItemViewModel(item))
				.collect(Collectors.toList());

		return new ResponseEntity<List<ItemViewModel>>(itemViewModel,HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ItemViewModel byId(@PathVariable Long id) {
		Optional<Item> item = this.itemService.getOne(id);

		if (!item.isPresent()) {
			throw new EntityNotFoundException();
		}

		ItemViewModel noteViewModel = this.mapper.convertToItemViewModel(item.get());

		return noteViewModel;
	}

	@GetMapping("/byProduct/{productId}")
	public List<ItemViewModel> bySpecies(@PathVariable Product productId) {
		List<Item> items = new ArrayList<>();
		items = this.itemService.getAllByProduct(productId);	
		List<ItemViewModel> productViewModel = items.stream()
				.map(item -> this.mapper.convertToItemViewModel(item))
				.collect(Collectors.toList());

		return productViewModel;
	}


	@PostMapping
	public ResponseEntity<Item> save(@RequestBody ItemViewModel itemCreateViewModel, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			throw new ValidationException();
		}

		Item itemEntity = this.mapper.convertToItemEntity(itemCreateViewModel);

		this.itemService.insertOrUpdate(itemEntity);

		return new ResponseEntity<Item>(itemEntity, HttpStatus.CREATED);
	}
	
	@PutMapping
	public Item update(@RequestBody ItemViewModel itemCreateViewModel, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			throw new ValidationException();
		}

		Item itemEntity = this.mapper.convertToItemEntity(itemCreateViewModel);

		this.itemService.insertOrUpdate(itemEntity);

		return itemEntity;
	}

	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id) {
		this.itemService.delete(id);
	}
	
}
