package com.grupoWeb.controller;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;
import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.grupoWeb.model.entity.Shipping;
import com.grupoWeb.service.ShippingService;

@RestController
@RequestMapping("/shipping")
public class ShippingController {
	
	@Autowired
	private ShippingService shippingService;
	
	@GetMapping
	public List<Shipping> all(){
		List<Shipping> shippings = this.shippingService.getAll();
		return shippings;
	}
	
	@GetMapping("/{id}")
	public Shipping byId(@PathVariable Long id) {
		Optional<Shipping> shipping = this.shippingService.getOne(id);
		if(!shipping.isPresent()) {
			throw new EntityNotFoundException();
		}
		return shipping.get();
	}
	
	@PostMapping
	public Shipping save(@RequestBody Shipping shipping,BindingResult bindingResult) {
		if(bindingResult.hasErrors()) {
			throw new ValidationException();
		}
		
		Shipping cityNew = this.shippingService.insertOrUpdate(shipping);
		return cityNew;
	}
	
	@PutMapping
	public Shipping update(@RequestBody Shipping shipping,BindingResult bindingResult) {
		if(bindingResult.hasErrors()) {
			throw new ValidationException();
		}
		
		Shipping shippingnew = this.shippingService.insertOrUpdate(shipping);
		return shippingnew;
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id) {
		this.shippingService.delete(id);
	}
}
