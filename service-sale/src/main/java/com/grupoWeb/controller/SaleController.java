package com.grupoWeb.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;
import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.grupoWeb.Mapper;
import com.grupoWeb.model.entity.Sale;
import com.grupoWeb.model.entity.Shipping;
import com.grupoWeb.service.SaleService;
import com.grupoWeb.service.ShippingService;
import com.grupoWeb.viewmodel.SaleViewModel;

@RestController
@RequestMapping("/sale")
public class SaleController {
	@Autowired
	private SaleService saleService;
	
	
	@Autowired
	private ShippingService shippingService;
	
	@Autowired
	private Mapper mapper;

	@GetMapping
	public ResponseEntity<List<SaleViewModel>> all(){
		List<SaleViewModel> saleViewModel = this.saleService.getAll().stream().map(sale->this.mapper.convertToSaleViewModel(sale)).collect(Collectors.toList());
		return new ResponseEntity<List<SaleViewModel>>(saleViewModel,HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public SaleViewModel byId(@PathVariable Long id) {
		Optional<Sale> sale = this.saleService.getOne(id);
		if(!sale.isPresent()) {
			throw new EntityNotFoundException();
		}
		SaleViewModel saleViewModel = this.mapper.convertToSaleViewModel(sale.get());
		return saleViewModel;
	}
	

	@GetMapping("/byShipping/{idShipping}")
	public List<SaleViewModel> byShipping(@PathVariable Long idShipping){
		List<Sale> sales = new ArrayList<>();
		Optional<Shipping> shipping = this.shippingService.getOne(idShipping);
		if(shipping.isPresent()) {
			sales = this.saleService.getAllByShipping(shipping.get());
		}
		List<SaleViewModel> saleViewModel = sales.stream().map(sale->this.mapper.convertToSaleViewModel(sale)).collect(Collectors.toList());
		return saleViewModel;
	}
	@PostMapping
	public Sale save(@RequestBody SaleViewModel saleViewModel, BindingResult bindingResult) {
		if(bindingResult.hasErrors()) {
			throw new ValidationException();
		}
		Sale sale = this.mapper.convertToSaleEntity(saleViewModel);
		this.saleService.insertOrUpdate(sale);
		return sale;
	}
	
	@PutMapping
	public Sale update(@RequestBody SaleViewModel saleViewModel, BindingResult bindingResult) {
		if(bindingResult.hasErrors()) {
			throw new ValidationException();
		}
		Sale addressEntity = this.mapper.convertToSaleEntity(saleViewModel);
		this.saleService.insertOrUpdate(addressEntity);
		return addressEntity;
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id) {
		this.saleService.delete(id);
	}
}
