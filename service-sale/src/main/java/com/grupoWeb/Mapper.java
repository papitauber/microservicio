package com.grupoWeb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.grupoWeb.client.ProductClientRest;
import com.grupoWeb.model.entity.Item;
import com.grupoWeb.model.entity.Product;
import com.grupoWeb.model.entity.Sale;
import com.grupoWeb.model.entity.Shipping;
import com.grupoWeb.model.repository.SaleRepository;
import com.grupoWeb.model.repository.ShippingRepository;
import com.grupoWeb.viewmodel.ItemViewModel;
import com.grupoWeb.viewmodel.SaleViewModel;

@Component
public class Mapper {
	@Autowired
	private SaleRepository saleRepository;
	
	@Autowired
	private ProductClientRest productClienteRest;
	
	//Mapper Item
	public ItemViewModel convertToItemViewModel(Item entity) {
		ItemViewModel viewModel = new ItemViewModel();
		viewModel.setIdItem(entity.getIdItem());
		viewModel.setQuantity(entity.getQuantity());
		viewModel.setProductId(entity.getProduct().getIdProduct());
		viewModel.setTotal(entity.getTotal());
		viewModel.setIdSale(entity.getSale().getIdSale());
		return viewModel;
	}
	

	public Item convertToItemEntity(ItemViewModel viewModel) {
		Sale sale = this.saleRepository.findById(viewModel.getIdSale()).get();
		Product product = this.productClienteRest.getProductById(viewModel.getProductId());
		Item entity = new Item(viewModel.getIdItem(),viewModel.getQuantity(),viewModel.getTotal(),product,sale);
		return entity;
	}


//Mapper Sale
	public SaleViewModel convertToSaleViewModel(Sale entity) {
		SaleViewModel viewModel = new SaleViewModel();
		viewModel.setIdSale(entity.getIdSale());
		viewModel.setDateSale(entity.getDateSale());
		viewModel.setIgv(entity.getIgv());
		viewModel.setTotal(entity.getTotal());
		viewModel.setIdShipping(entity.getShipping().getIdShipping());
		return viewModel;
	}
	
	@Autowired
	private ShippingRepository shippingRepository;

	
	public Sale convertToSaleEntity(SaleViewModel viewModel) {
		Shipping shipping = this.shippingRepository.findById(viewModel.getIdShipping()).get();
		Sale entity = new Sale(viewModel.getIdSale(),viewModel.getDateSale(),viewModel.getTotal(),viewModel.getIgv(),shipping);
		return entity;
	}
}
