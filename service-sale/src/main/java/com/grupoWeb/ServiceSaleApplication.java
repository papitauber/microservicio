package com.grupoWeb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@RibbonClient("service-product-catalog")
@EnableEurekaClient
@EnableFeignClients
@SpringBootApplication
public class ServiceSaleApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceSaleApplication.class, args);
	}

}
