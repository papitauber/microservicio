package com.grupoWeb.viewmodel;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class ShippingViewModel {
	
	private Long idShipping;
	@NotNull
	@Min(3)
	private String name;
	@NotNull
	private Double priceShipping;
	@NotNull
	private Double timeArrival;
	public Long getIdShipping() {
		return idShipping;
	}
	public void setIdShipping(Long idShipping) {
		this.idShipping = idShipping;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getPriceShipping() {
		return priceShipping;
	}
	public void setPriceShipping(Double priceShipping) {
		this.priceShipping = priceShipping;
	}
	public Double getTimeArrival() {
		return timeArrival;
	}
	public void setTimeArrival(Double timeArrival) {
		this.timeArrival = timeArrival;
	}
}
