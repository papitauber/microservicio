package com.grupoWeb.viewmodel;

import java.util.Date;

import javax.validation.constraints.NotNull;

public class SaleViewModel {
	
	private Long idSale;
	@NotNull
	private Date dateSale;
	private Long idShipping;
	
	private Double total;
	
	private Double igv;
	
	public Long getIdSale() {
		return idSale;
	}
	public void setIdSale(Long idSale) {
		this.idSale = idSale;
	}
	public Date getDateSale() {
		return dateSale;
	}
	public void setDateSale(Date dateSale) {
		this.dateSale = dateSale;
	}


	public Long getIdShipping() {
		return idShipping;
	}
	public void setIdShipping(Long idShipping) {
		this.idShipping = idShipping;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public Double getIgv() {
		return igv;
	}
	public void setIgv(Double igv) {
		this.igv = igv;
	}

	
	
}
