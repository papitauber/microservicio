package com.grupoWeb.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.grupoWeb.model.entity.Item;
import com.grupoWeb.model.entity.Product;
import com.grupoWeb.model.entity.Sale;


@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {

	List<Item> findAllByProduct(Product product);

	Item findPriceByProduct(Product product);
	
	@Query("SELECT s FROM Item s WHERE s.sale.idSale=?1")
	List<Item> findAllBySale(Long idSale);
}