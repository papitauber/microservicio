package com.grupoWeb.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.grupoWeb.model.entity.Item;
import com.grupoWeb.model.entity.Sale;
import com.grupoWeb.model.entity.Shipping;


@Repository
public interface SaleRepository extends JpaRepository<Sale, Long>{

	List<Sale> findAllByShipping(Shipping shipping);

	//List<Item> findAllBySale(Sale sale);
}
