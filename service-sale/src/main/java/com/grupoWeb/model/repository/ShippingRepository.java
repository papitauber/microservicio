package com.grupoWeb.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.grupoWeb.model.entity.Shipping;

@Repository
public interface ShippingRepository extends JpaRepository<Shipping, Long>{

}
