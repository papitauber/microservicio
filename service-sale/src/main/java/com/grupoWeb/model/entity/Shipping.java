package com.grupoWeb.model.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "shippings")
public class Shipping {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long idShipping;
	private String name;
	private Double priceShipping;
	private Double timeArrival;
	
	@OneToOne(fetch = FetchType.LAZY, mappedBy = "shipping", cascade = CascadeType.ALL)
	@JsonIgnore
	private Sale sale;
	
	public Shipping() {
		super();
	}
	
	public Shipping(Long idShipping, String name, Double priceShipping, Double timeArrival) {
		super();
		this.idShipping = idShipping;
		this.name = name;
		this.priceShipping = priceShipping;
		this.timeArrival = timeArrival;
	}
	public Long getIdShipping() {
		return idShipping;
	}
	public void setIdShipping(Long idShipping) {
		this.idShipping = idShipping;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getPriceShipping() {
		return priceShipping;
	}
	public void setPriceShipping(Double priceShipping) {
		this.priceShipping = priceShipping;
	}
	public Double getTimeArrival() {
		return timeArrival;
	}
	public void setTimeArrival(Double timeArrival) {
		this.timeArrival = timeArrival;
	}

	public Sale getSale() {
		return sale;
	}

	public void setSale(Sale sale) {
		this.sale = sale;
	}
}
