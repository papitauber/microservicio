package com.grupoWeb.model.entity;



import javax.persistence.CascadeType;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "items")
public class Item {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idItem;
	private Integer quantity;
	private Double total;
	@ManyToOne
	@JoinColumn(name = "product_id")
	private Product product;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "sale_id")
    private Sale sale;
	
	public Item() {
		super();
		
	}


	public Item(Long idItem, Integer quantity, Double total, Product product, Sale sale) {
		super();
		this.idItem = idItem;
		this.quantity = quantity;
		this.total = total;
		this.product = product;
		this.sale = sale;
	}






	public Double getTotal() {
		return total;
	}


	public void setTotal(Double total) {
		this.total = total;
	}




	public Long getIdItem() {
		return idItem;
	}

	public void setIdItem(Long idItem) {
		this.idItem = idItem;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Product getProduct() {
		return product;
	}


	public void setProduct(Product product) {
		this.product = product;
	}


	public Sale getSale() {
		return sale;
	}


	public void setSale(Sale sale) {
		this.sale = sale;
	}


}
