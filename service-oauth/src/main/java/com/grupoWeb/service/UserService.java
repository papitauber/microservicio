package com.grupoWeb.service;

import com.grupoWeb.model.entity.Account;


public interface UserService {
	public Account findByUsername(String username);
}