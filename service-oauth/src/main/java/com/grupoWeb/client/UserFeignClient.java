package com.grupoWeb.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.grupoWeb.model.entity.Account;

@FeignClient(name="service-user-management", url="localhost:9010")
public interface UserFeignClient {
	@GetMapping("/users/search/getUsername")
	Account findByUserName(@RequestParam String username);
}
