package com.grupoWeb.api.controller;


import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;
import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.grupoWeb.Mapper;
import com.grupoWeb.api.viewmodel.CategoryViewModel;
import com.grupoWeb.model.entity.Category;
import com.grupoWeb.service.CategoryService;


@RestController
@RequestMapping("/category")
public class CategoryController {

	@Autowired
	private CategoryService categoryService;
		
	@Autowired
	private Mapper mapper;

	@GetMapping
	public List<CategoryViewModel> all() {
		List<Category> categories = this.categoryService.getAll();

		// map from entity to view model
		List<CategoryViewModel> productViewModel = categories.stream().map(category -> this.mapper.convertToCategoryViewModel(category))
				.collect(Collectors.toList());

		return productViewModel;
	}

	@GetMapping("/{id}")
	public CategoryViewModel byId(@PathVariable Long id) {
		Optional<Category> category = this.categoryService.getOne(id);

		if (!category.isPresent()) {
			throw new EntityNotFoundException();
		}

		CategoryViewModel noteViewModel = this.mapper.convertToCategoryViewModel(category.get());

		return noteViewModel;
	}

	@PostMapping
	public Category save(@RequestBody CategoryViewModel categoryCreateViewModel, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			throw new ValidationException();
		}

		Category itemEntity = this.mapper.convertToCategoryEntity(categoryCreateViewModel);

		this.categoryService.insertOrUpdate(itemEntity);

		return itemEntity;
	}
	
	@PutMapping
	public Category update(@RequestBody CategoryViewModel categoryCreateViewModel, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			throw new ValidationException();
		}

		Category itemEntity = this.mapper.convertToCategoryEntity(categoryCreateViewModel);

		this.categoryService.insertOrUpdate(itemEntity);

		return itemEntity;
	}

	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id) {
		this.categoryService.delete(id);
	}
	
}
