package com.grupoWeb.api.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;
import javax.validation.ValidationException;

import org.springframework.core.env.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.grupoWeb.Mapper;
import com.grupoWeb.api.viewmodel.ProductViewModel;
import com.grupoWeb.model.entity.Product;
import com.grupoWeb.service.ProductService;


@RestController
@RequestMapping("/product")
public class ProductController {
	@Autowired
	private Environment env;
	
	@Autowired
	private ProductService productService;
		
	@Autowired
	private Mapper mapper;

	@GetMapping
	public ResponseEntity<List<ProductViewModel>> getProducts() {
		//List<Product> products = this.productService.getAll();
		// map from entity to view model
		List<ProductViewModel> productViewModel =this.productService.getAll().stream().
				map(product -> this.mapper.convertToProductViewModel(product))
				.collect(Collectors.toList());

		return new ResponseEntity<List<ProductViewModel>>(productViewModel,HttpStatus.OK) ;
	}

	@GetMapping("/{id}")
	public ResponseEntity<ProductViewModel> getProductById(@PathVariable Long id) {
		Optional<Product> product = this.productService.getOne(id);

		if (!product.isPresent()) {
			throw new EntityNotFoundException();
		}

		ProductViewModel noteViewModel = this.mapper.convertToProductViewModel(product.get());

		return ResponseEntity.ok(noteViewModel);
	}

	@GetMapping("/byUserProfile/{idUserProfile}")
	public List<ProductViewModel> byUserProfile(@PathVariable Long idUserProfile){
		List<Product> products = new ArrayList<>();
		products = this.productService.getAllByUser(idUserProfile);
		List<ProductViewModel> saleViewModel = products.stream().map(product->this.mapper.convertToProductViewModel(product)).collect(Collectors.toList());
		return saleViewModel;
	}
	@PostMapping
	public ResponseEntity<Product> save(@RequestBody ProductViewModel productCreateViewModel, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			throw new ValidationException();
		}

		Product productEntity = this.mapper.convertToProductEntity(productCreateViewModel);

		this.productService.insertOrUpdate(productEntity);

		return new ResponseEntity<Product>(productEntity,HttpStatus.CREATED);
	}
	
	@PutMapping
	public Product update(@RequestBody ProductViewModel productCreateViewModel, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			throw new ValidationException();
		}

		Product productEntity = this.mapper.convertToProductEntity(productCreateViewModel);

		this.productService.insertOrUpdate(productEntity);

		return productEntity;
	}

	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id) {
		this.productService.delete(id);
	}
	
}
