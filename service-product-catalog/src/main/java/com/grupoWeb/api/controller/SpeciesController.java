package com.grupoWeb.api.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;
import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.grupoWeb.Mapper;
import com.grupoWeb.api.viewmodel.SpeciesViewModel;
import com.grupoWeb.model.entity.Category;
import com.grupoWeb.model.entity.Species;
import com.grupoWeb.service.CategoryService;
import com.grupoWeb.service.SpeciesService;

@RestController
@RequestMapping("/species")
public class SpeciesController {
	@Autowired
	private SpeciesService speciesService;
		
	@Autowired
	private Mapper mapper;

	@GetMapping
	public List<SpeciesViewModel> all() {
		List<Species> categories = this.speciesService.getAll();

		// map from entity to view model
		List<SpeciesViewModel> productViewModel = categories.stream().map(category -> this.mapper.convertToSpeciesViewModel(category))
				.collect(Collectors.toList());

		return productViewModel;
	}

	@GetMapping("/{id}")
	public SpeciesViewModel byId(@PathVariable Long id) {
		Optional<Species> category = this.speciesService.getOne(id);

		if (!category.isPresent()) {
			throw new EntityNotFoundException();
		}

		SpeciesViewModel noteViewModel = this.mapper.convertToSpeciesViewModel(category.get());

		return noteViewModel;
	}

	@Autowired
	private CategoryService categoryService;
	
	@PostMapping
	public Species save(@RequestBody SpeciesViewModel speciesCreateViewModel, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			throw new ValidationException();
		}
		Optional<Category> category = this.categoryService.getOne(speciesCreateViewModel.getIdCategory());
		Species speciesEntity = this.mapper.convertToSpeciesEntity(speciesCreateViewModel);
		speciesEntity.setCategory(category.get());;
		this.speciesService.insertOrUpdate(speciesEntity);

		return speciesEntity;
	}
	
	@PutMapping
	public Species update(@RequestBody SpeciesViewModel speciesCreateViewModel, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			throw new ValidationException();
		}

		Species speciesEntity = this.mapper.convertToSpeciesEntity(speciesCreateViewModel);

		this.speciesService.insertOrUpdate(speciesEntity);

		return speciesEntity;
	}

	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id) {
		this.speciesService.delete(id);
	}
}
