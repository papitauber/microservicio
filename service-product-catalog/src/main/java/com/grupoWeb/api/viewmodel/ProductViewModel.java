package com.grupoWeb.api.viewmodel;

import java.util.Date;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class ProductViewModel {
	private Long idProduct;
	private String name;
	@NotNull
	@Min(5)
	@Max(250)
	private String description;
	@NotNull
	private Date dateCollection;
	@NotNull
	private Long stock;

	@NotNull
	private Double price;
	@NotNull
	private String image;

	@NotNull
	private Long idSpecies;
	@NotNull
	private Long idUserProfile;

	public Long getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(Long idProduct) {
		this.idProduct = idProduct;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDateCollection() {
		return dateCollection;
	}

	public void setDateCollection(Date dateCollection) {
		this.dateCollection = dateCollection;
	}


	public Long getStock() {
		return stock;
	}

	public void setStock(Long stock) {
		this.stock = stock;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Long getIdSpecies() {
		return idSpecies;
	}

	public void setIdSpecies(Long idSpecies) {
		this.idSpecies = idSpecies;
	}

	public Long getIdUserProfile() {
		return idUserProfile;
	}

	public void setIdUserProfile(Long idUserProfile) {
		this.idUserProfile = idUserProfile;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
