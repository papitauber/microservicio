package com.grupoWeb.api.viewmodel;

import javax.validation.constraints.Min;

import com.sun.istack.NotNull;

public class SpeciesViewModel {

	private Long idSpecies;
	
	@NotNull
	@Min(3)
	private String name;
	
	@NotNull
	private Long idCategory;

	public Long getIdSpecies() {
		return idSpecies;
	}

	public void setIdSpecies(Long idSpecies) {
		this.idSpecies = idSpecies;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getIdCategory() {
		return idCategory;
	}

	public void setIdCategory(Long idCategory) {
		this.idCategory = idCategory;
	}
}
