package com.grupoWeb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.grupoWeb.api.viewmodel.CategoryViewModel;
import com.grupoWeb.api.viewmodel.ProductViewModel;
import com.grupoWeb.api.viewmodel.SpeciesViewModel;
import com.grupoWeb.model.entity.Category;
import com.grupoWeb.model.entity.Product;
import com.grupoWeb.model.entity.Species;
import com.grupoWeb.model.repository.CategoryRepository;
import com.grupoWeb.model.repository.SpeciesRepository;

@Component
public class Mapper {

	//Mapper Category
	public CategoryViewModel convertToCategoryViewModel(Category entity) {
		CategoryViewModel viewModel = new CategoryViewModel();
		viewModel.setIdCategory(entity.getIdCategory());
		viewModel.setName(entity.getName());
		return viewModel;
	}
	
	public Category convertToCategoryEntity(CategoryViewModel viewModel) {
		Category entity = new Category(viewModel.getIdCategory(),viewModel.getName());
		return entity;
	}
	
	//Mapper Species
	public SpeciesViewModel convertToSpeciesViewModel(Species entity) {
		SpeciesViewModel viewModel = new SpeciesViewModel();
		viewModel.setIdSpecies(entity.getIdSpecies());
		viewModel.setName(entity.getName());
		viewModel.setIdCategory(entity.getCategory().getIdCategory());
		return viewModel;
	}
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	public Species convertToSpeciesEntity(SpeciesViewModel viewModel) {
		Category category = this.categoryRepository.findById(viewModel.getIdCategory()).get();
		Species entity = new Species(viewModel.getIdSpecies(),viewModel.getName(),category);
		return entity;
	}
	
	@Autowired
	private SpeciesRepository speciesRepository;
	
	//Mapper Product
	public ProductViewModel convertToProductViewModel(Product entity) {
		ProductViewModel viewModel = new ProductViewModel();
		viewModel.setIdProduct(entity.getIdProduct());
		viewModel.setName(entity.getName());
		viewModel.setDateCollection(entity.getDateCollection());
		viewModel.setDescription(entity.getDescription());
		viewModel.setPrice(entity.getPrice());
		viewModel.setStock(entity.getStock());
		viewModel.setImage(entity.getImage());
		viewModel.setIdUserProfile(entity.getIdUserProfile());
		viewModel.setIdSpecies(entity.getSpecies().getIdSpecies());
		return viewModel;
	}
	
	
	public Product convertToProductEntity(ProductViewModel viewModel) {
		Species species = this.speciesRepository.findById(viewModel.getIdSpecies()).get();
		Product entity = new Product(viewModel.getIdProduct(),viewModel.getName(),viewModel.getDescription(),viewModel.getDateCollection(),
				viewModel.getStock(),viewModel.getPrice(),viewModel.getImage(),species,viewModel.getIdUserProfile());
		return entity;
	}
}
