package com.grupoWeb.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="products")
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "product_id")
	private Long idProduct;
	private String name;
	private String description;
	
	private Date dateCollection;
	private Long stock;
	private Double price;
	private String image;
	
	@ManyToOne
	@JoinColumn(name = "id_species")
	private Species species;
	
	private Long idUserProfile;

	@Transient
	private Integer port;
	public Product() {
		super();
	}


	public Product(Long idProduct, String name, String description, Date dateCollection, Long stock, Double price,
			String image, Species species, Long idUserProfile) {
		super();
		this.idProduct = idProduct;
		this.name = name;
		this.description = description;
		this.dateCollection = dateCollection;
		this.stock = stock;
		this.price = price;
		this.image = image;
		this.species = species;
		this.idUserProfile = idUserProfile;
	}


	public Long getIdUserProfile() {
		return idUserProfile;
	}



	public void setIdUserProfile(Long idUserProfile) {
		this.idUserProfile = idUserProfile;
	}



	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Long getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(Long idProduct) {
		this.idProduct = idProduct;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDateCollection() {
		return dateCollection;
	}

	public void setDateCollection(Date dateCollection) {
		this.dateCollection = dateCollection;
	}



	public Long getStock() {
		return stock;
	}


	public void setStock(Long stock) {
		this.stock = stock;
	}


	public Double getPrice() {
		return price;
	}


	public void setPrice(Double price) {
		this.price = price;
	}


	public String getImage() {
		return image;
	}


	public void setImage(String image) {
		this.image = image;
	}


	public Species getSpecies() {
		return species;
	}

	public void setSpecies(Species species) {
		this.species = species;
	}


	public Integer getPort() {
		return port;
	}


	public void setPort(Integer port) {
		this.port = port;
	}

	
}
