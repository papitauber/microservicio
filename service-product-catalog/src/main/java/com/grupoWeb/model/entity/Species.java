package com.grupoWeb.model.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="species")
public class Species {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long idSpecies;
	@Column(name = "name")
	private String name;
	
	@ManyToOne
	private Category category;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "species", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<Product> products;
	
	public Species() {
		products = new ArrayList<Product>();
	}
	


	public Species(Long idSpecies, String name, Category category) {
		this.idSpecies = idSpecies;
		this.name = name;
		this.category = category;
	}



	public Long getIdSpecies() {
		return idSpecies;
	}

	public void setIdSpecies(Long idSpecies) {
		this.idSpecies = idSpecies;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



	public Category getCategory() {
		return category;
	}



	public void setCategory(Category category) {
		this.category = category;
	}



	public List<Product> getProducts() {
		return products;
	}



	public void setProducts(List<Product> products) {
		this.products = products;
	}
}
