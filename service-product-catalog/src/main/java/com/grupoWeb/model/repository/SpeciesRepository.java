package com.grupoWeb.model.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import com.grupoWeb.model.entity.Species;

@Repository
public interface SpeciesRepository extends JpaRepository<Species, Long>{
/*
	@Query("SELECT s.id FROM Species s WHERE s.idCategory?=1")
	Long findByCategory(Long idCategory);*/
}
