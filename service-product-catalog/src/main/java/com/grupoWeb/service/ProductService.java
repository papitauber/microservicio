package com.grupoWeb.service;

import java.util.List;

import com.grupoWeb.model.entity.Product;

public interface ProductService extends CRUDService<Product> {

	List<Product> getAllByUser(Long idUser);
}
