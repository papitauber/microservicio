package com.grupoWeb.service;

import com.grupoWeb.model.entity.Category;

public interface CategoryService extends CRUDService<Category> {

}
