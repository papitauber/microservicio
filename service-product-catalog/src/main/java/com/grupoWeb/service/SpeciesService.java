package com.grupoWeb.service;

import com.grupoWeb.model.entity.Species;

public interface SpeciesService extends CRUDService<Species>{

}
