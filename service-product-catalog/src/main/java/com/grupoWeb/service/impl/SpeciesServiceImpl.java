package com.grupoWeb.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grupoWeb.model.entity.Species;
import com.grupoWeb.model.repository.SpeciesRepository;
import com.grupoWeb.service.SpeciesService;

@Service
public class SpeciesServiceImpl implements SpeciesService{

	@Autowired
	private SpeciesRepository speciesRepository;
	
	@Transactional
	@Override
	public Species insertOrUpdate(Species entity) {
		// TODO Auto-generated method stub
		return speciesRepository.save(entity);
	}

	@Override
	public Optional<Species> getOne(Long id) {
		// TODO Auto-generated method stub
		return speciesRepository.findById(id);
	}

	@Override
	public List<Species> getAll() {
		// TODO Auto-generated method stub
		return speciesRepository.findAll();
	}

	@Transactional
	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		speciesRepository.deleteById(id);
	}

}
