package com.grupoWeb.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grupoWeb.model.entity.Product;
import com.grupoWeb.model.repository.ProductRepository;
import com.grupoWeb.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService{

	@Autowired
	private ProductRepository productRepository;
	
	@Transactional
	@Override
	public Product insertOrUpdate(Product entity) {
		// TODO Auto-generated method stub
		return productRepository.save(entity);
	}

	@Override
	public Optional<Product> getOne(Long id) {
		// TODO Auto-generated method stub
		return productRepository.findById(id);
	}

	@Override
	public List<Product> getAll() {
		// TODO Auto-generated method stub
		List<Product> products = new ArrayList<>();
		productRepository.findAll().iterator().forEachRemaining(products::add);
		return products;
	}

	@Transactional
	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		productRepository.deleteById(id);
	}

	@Override
	public List<Product> getAllByUser(Long idUser) {
		// TODO Auto-generated method stub
		return productRepository.findAllByUser(idUser);
	}

}
