package com.grupoWeb.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grupoWeb.model.entity.Category;
import com.grupoWeb.model.repository.CategoryRepository;
import com.grupoWeb.service.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService{

	@Autowired
	private CategoryRepository categoryRepository;
	
	
	@Transactional
	@Override
	public Category insertOrUpdate(Category entity) {
		// TODO Auto-generated method stub
		return categoryRepository.save(entity);
	}

	@Override
	public Optional<Category> getOne(Long id) {
		// TODO Auto-generated method stub
		return categoryRepository.findById(id);
	}

	@Override
	public List<Category> getAll() {
		// TODO Auto-generated method stub
		return categoryRepository.findAll();
	}

	@Transactional
	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		categoryRepository.deleteById(id);
	}

}
