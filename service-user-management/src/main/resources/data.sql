INSERT INTO users (username,password,enabled,email) VALUES ('hampcode','$2a$10$VzsBpvJXVkXaAy29XooHZORsE6IzF.2ZZMB1drnURqbCyhWEazkdK',true,'hmendo81@gmail.com');

INSERT INTO users (username,password,enabled,email)VALUES ('usercode','$2a$10$R8CUVucLB2B7omTl8H0CPuSPqxquNKR2JuUAIcg.kquUT3xxnROdi',true,'usercode@gmail.com');


INSERT INTO rol (name) VALUES ('ROLE_ADMIN');
INSERT INTO rol (name) VALUES ('ROLE_USER');


INSERT INTO user_roles (user_id,rol_id) VALUES (1,1);
INSERT INTO user_roles (user_id,rol_id) VALUES (1,2);
INSERT INTO user_roles (user_id,rol_id) VALUES (2,1);
