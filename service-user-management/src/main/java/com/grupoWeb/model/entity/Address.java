package com.grupoWeb.model.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "address")
public class Address {
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
	private Long idAddress;
    @Column(name = "typeRoad")
	private Integer typeRoad;
    @Column(name = "nameAddress")
	private String nameAddress;
    @Column(name = "numberAddress")
	private Integer numberAddress;
    @Column(name = "blockNumber")
	private String blockNumber;
    
	@ManyToOne(cascade = CascadeType.ALL)
	private District district;
	
	@ManyToOne
	private UserProfile userProfile;

	public Address() {
		super();
	}
	
	public Address(Long idAddress, Integer typeRoad, String nameAddress, Integer numberAddress, String blockNumber,
			District district, UserProfile userProfile) {
		super();
		this.idAddress = idAddress;
		this.typeRoad = typeRoad;
		this.nameAddress = nameAddress;
		this.numberAddress = numberAddress;
		this.blockNumber = blockNumber;
		this.district = district;
		this.userProfile = userProfile;
	}

	public Long getIdAddress() {
		return idAddress;
	}

	public void setIdAddress(Long idAddress) {
		this.idAddress = idAddress;
	}

	public Integer getTypeRoad() {
		return typeRoad;
	}

	public void setTypeRoad(Integer typeRoad) {
		this.typeRoad = typeRoad;
	}

	public String getNameAddress() {
		return nameAddress;
	}

	public void setNameAddress(String nameAddress) {
		this.nameAddress = nameAddress;
	}

	public Integer getNumberAddress() {
		return numberAddress;
	}

	public void setNumberAddress(Integer numberAddress) {
		this.numberAddress = numberAddress;
	}

	public String getBlockNumber() {
		return blockNumber;
	}

	public void setBlockNumber(String blockNumber) {
		this.blockNumber = blockNumber;
	}

	public District getDistrict() {
		return district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

	public UserProfile getUserProfile() {
		return userProfile;
	}

	public void setUserProfile(UserProfile userProfile) {
		this.userProfile = userProfile;
	}
	
	
}
