package com.grupoWeb.model.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="district")
public class District {
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idDistric;
	private String name;
	

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "district", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<Address> addresses;
	
	public District() {
		addresses=new ArrayList<Address>();
	}

	public District(Long idDistric, String name) {
		super();
		this.idDistric = idDistric;
		this.name = name;
	}

	public Long getIdDistric() {
		return idDistric;
	}

	public void setIdDistric(Long idDistric) {
		this.idDistric = idDistric;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}
}
