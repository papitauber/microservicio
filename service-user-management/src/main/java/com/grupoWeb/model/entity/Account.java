package com.grupoWeb.model.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="users")
public class Account {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "")
	private Long idUser;
	@Column(name = "email")
	private String email;
	@Column(name = "password")
	private String password;
	private Boolean enabled;
	private String username;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "account", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<UserProfile> userProfiles;
	
	
	@JoinTable(name="user_roles",
			joinColumns = @JoinColumn(name="user_id"),
			inverseJoinColumns = @JoinColumn(name="rol_id"),
			uniqueConstraints = {@UniqueConstraint(
					columnNames = {"user_id","rol_id"})})
	@ManyToMany(fetch = FetchType.LAZY)
	private List<Rol> rols;

	public Account() {
		userProfiles = new ArrayList<UserProfile>();
	}



	public Account(Long idUser, String email, String password, Boolean enabled, String username) {
		super();
		this.idUser = idUser;
		this.email = email;
		this.password = password;
		this.enabled = enabled;
		this.username = username;
	}



	public Long getIdUser() {
		return idUser;
	}
	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public List<UserProfile> getUserProfiles() {
		return userProfiles;
	}
	public void setUserProfiles(List<UserProfile> userProfiles) {
		this.userProfiles = userProfiles;
	}

	public Boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<Rol> getRols() {
		return rols;
	}


	public void setRols(List<Rol> rols) {
		this.rols = rols;
	}
	
	
}
