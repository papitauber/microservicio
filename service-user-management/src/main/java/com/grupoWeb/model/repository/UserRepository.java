package com.grupoWeb.model.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.grupoWeb.model.entity.Account;

import feign.Param;

@RepositoryRestResource(collectionResourceRel = "users",path="users")
public interface UserRepository extends PagingAndSortingRepository<Account, Long>{

	@Query("SELECT u FROM Account u WHERE u.username=?1")
	@RestResource(path="getUsername")
	Account findByUserName(/*@Param("username")*/ String username);
	
	@Query("SELECT u FROM Account u WHERE u.password=?1")
	Account findByPassword(String password);
	
	@Query("SELECT u.id FROM Account u WHERE u.username=?1")
	Long findIdByUserName(String username);
	
}
