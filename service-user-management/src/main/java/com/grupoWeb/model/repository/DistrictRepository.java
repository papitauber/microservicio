package com.grupoWeb.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.grupoWeb.model.entity.District;
@Repository
public interface DistrictRepository extends JpaRepository<District, Long>{

}
