package com.grupoWeb.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grupoWeb.model.entity.District;
import com.grupoWeb.model.repository.DistrictRepository;
import com.grupoWeb.service.DistrictService;

@Service
public class DistrictServiceImpl implements DistrictService {

	@Autowired
	private DistrictRepository districtRepository;
	
	@Transactional
	@Override
	public District insertOrUpdate(District entity) {
		// TODO Auto-generated method stub
		return districtRepository.save(entity);
	}

	@Override
	public Optional<District> getOne(Long id) {
		// TODO Auto-generated method stub
		return districtRepository.findById(id);
	}

	@Override
	public List<District> getAll() {
		// TODO Auto-generated method stub
		return districtRepository.findAll();
	}

	@Transactional
	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		districtRepository.deleteById(id);
	}

}
