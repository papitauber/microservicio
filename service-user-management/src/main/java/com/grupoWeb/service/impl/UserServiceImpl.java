package com.grupoWeb.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grupoWeb.model.entity.Account;
import com.grupoWeb.model.repository.UserRepository;
import com.grupoWeb.service.UserService;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserRepository userRepository;
	
	//@Autowired
	//private BCryptPasswordEncoder passwordEncoder;
	
	@Transactional
	@Override
	public Account insertOrUpdate(Account entity) {
		// TODO Auto-generated method stub
		//String passwordCrypt=passwordEncoder.encode(entity.getPassword());
		//entity.setPassword(passwordEncoder.encode(entity.getPassword()));
		return userRepository.save(entity);
	}

	@Override
	public Optional<Account> getOne(Long id) {
		// TODO Auto-generated method stub
		return userRepository.findById(id);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		userRepository.deleteById(id);
	}

	@Override
	public List<Account> getAll() {
		// TODO Auto-generated method stub
		return (List<Account>) userRepository.findAll();
	}

	@Override
	public Account getUsername(String username) {
		// TODO Auto-generated method stub
		return userRepository.findByUserName(username);
	}

	@Override
	public Account getPassword(String password) {
		// TODO Auto-generated method stub
		return userRepository.findByPassword(password);
	}

	@Override
	public Long getIdByUserName(String username) {
		// TODO Auto-generated method stub
		return userRepository.findIdByUserName(username);
	}
	
}
