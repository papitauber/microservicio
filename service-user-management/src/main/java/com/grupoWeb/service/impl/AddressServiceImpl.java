package com.grupoWeb.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grupoWeb.model.entity.Address;
import com.grupoWeb.model.repository.AddressRepository;
import com.grupoWeb.service.AddressService;

@Service
public class AddressServiceImpl implements AddressService{
	@Autowired
	private AddressRepository addressRepository;
	
	@Transactional
	@Override
	public Address insertOrUpdate(Address entity) {
		// TODO Auto-generated method stub
		return addressRepository.save(entity);
	}

	@Override
	public Optional<Address> getOne(Long id) {
		// TODO Auto-generated method stub
		return addressRepository.findById(id);
	}

	@Override
	public List<Address> getAll() {
		// TODO Auto-generated method stub
		return addressRepository.findAll();
	}

	@Transactional
	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		addressRepository.deleteById(id);
	}

}
