package com.grupoWeb.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grupoWeb.model.entity.UserProfile;
import com.grupoWeb.model.repository.UserProfileRepository;
import com.grupoWeb.service.UserProfileService;

@Service
public class UserProfileServiceImpl implements UserProfileService{

	@Autowired
	private UserProfileRepository userProfileRepository;
	
	@Transactional
	@Override
	public UserProfile insertOrUpdate(UserProfile entity) {
		// TODO Auto-generated method stub
		return userProfileRepository.save(entity);
	}

	@Override
	public Optional<UserProfile> getOne(Long id) {
		// TODO Auto-generated method stub
		return userProfileRepository.findById(id);
	}

	@Override
	public List<UserProfile> getAll() {
		// TODO Auto-generated method stub
		return userProfileRepository.findAll();
	}

	@Transactional
	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		userProfileRepository.deleteById(id);
	}
	
}
