package com.grupoWeb.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grupoWeb.model.entity.Rol;
import com.grupoWeb.model.repository.RolRepository;
import com.grupoWeb.service.RolService;

@Service
public class RolServiceImpl implements RolService{

	@Autowired
	private RolRepository rolRepository;
	
	@Transactional
	@Override
	public Rol insertOrUpdate(Rol entity) {
		// TODO Auto-generated method stub
		return rolRepository.save(entity);
	}

	@Override
	public Optional<Rol> getOne(Long id) {
		// TODO Auto-generated method stub
		return rolRepository.findById(id);
	}

	@Override
	public List<Rol> getAll() {
		// TODO Auto-generated method stub
		return rolRepository.findAll();
	}

	@Transactional
	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		rolRepository.deleteById(id);
	}

}
