package com.grupoWeb.service;

import com.grupoWeb.model.entity.Account;

public interface UserService extends CRUDService<Account> {
	Account getUsername(String username);
	
	Account getPassword(String password);
	
	Long getIdByUserName(String username);
}
