package com.grupoWeb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.grupoWeb.api.viewmodel.AddressViewModel;
import com.grupoWeb.api.viewmodel.DistrictViewModel;
import com.grupoWeb.api.viewmodel.RolViewModel;
import com.grupoWeb.api.viewmodel.UserProfileViewModel;
import com.grupoWeb.api.viewmodel.UserViewModel;
import com.grupoWeb.model.entity.Account;
import com.grupoWeb.model.entity.Address;
import com.grupoWeb.model.entity.District;
import com.grupoWeb.model.entity.Rol;
import com.grupoWeb.model.entity.UserProfile;
import com.grupoWeb.model.repository.DistrictRepository;
import com.grupoWeb.model.repository.RolRepository;
import com.grupoWeb.model.repository.UserProfileRepository;
import com.grupoWeb.model.repository.UserRepository;

@Component
public class Mapper {
	//Mapper Address	
		@Autowired
		private DistrictRepository districtRepository;
		
		@Autowired
		private UserProfileRepository userProfileRepository;
		
		public AddressViewModel convertToAddressViewModel(Address entity) {
			AddressViewModel viewModel = new AddressViewModel();
			viewModel.setIdAddress(entity.getIdAddress());
			viewModel.setBlockNumber(entity.getBlockNumber());
			viewModel.setIdDistrict(entity.getDistrict().getIdDistric());
			viewModel.setNameAddress(entity.getNameAddress());
			viewModel.setNumberAddress(entity.getNumberAddress());
			viewModel.setTypeRoad(entity.getTypeRoad());
			viewModel.setIdProfile(entity.getUserProfile().getIdProfile());
			return viewModel;
		}
		

		public Address converToAddressEntity(AddressViewModel viewModel) {
			District district = this.districtRepository.findById(viewModel.getIdDistrict()).get();
			UserProfile profile = this.userProfileRepository.findById(viewModel.getIdProfile()).get();
			Address entity = new Address(viewModel.getIdAddress(),viewModel.getNumberAddress(),viewModel.getNameAddress()
					,viewModel.getTypeRoad(),viewModel.getBlockNumber(),district,profile);
			return entity;
		}

		//Mapper Rol
		public RolViewModel convertToRolViewModel(Rol entity) {
			RolViewModel viewModel = new RolViewModel();
			viewModel.setIdRol(entity.getIdRol());
			viewModel.setName(entity.getName());
			return viewModel;
		}
		public Rol convertToRolEntity(RolViewModel viewModel) {
			Rol entity = new Rol(viewModel.getIdRol(),viewModel.getName());
			return entity;
		}
		
		//Mapper District
		public DistrictViewModel convertToDistrictViewModel(District entity) {
			DistrictViewModel viewModel = new DistrictViewModel();
			viewModel.setIdDistric(entity.getIdDistric());
			viewModel.setName(entity.getName());
			return viewModel;
		}
		
		public District convertToDistrictEntity(DistrictViewModel viewModel) {
			District entity = new District(viewModel.getIdDistric(),viewModel.getName());
			return entity;
		}
		
		//Mapper User
		@Autowired
		private RolRepository rolRepository;
		
		public UserViewModel convertToUserViewModel(Account entity) {
			UserViewModel viewModel = new UserViewModel();
			viewModel.setIdUser(entity.getIdUser());
			viewModel.setEmail(entity.getEmail());
			viewModel.setPassword(entity.getPassword());
			viewModel.setEnabled(entity.getEnabled());
			viewModel.setUsername(entity.getUsername());
			viewModel.setRols(entity.getRols());
			return viewModel;
		}
		
		public Account convertToUserEntity(UserViewModel viewModel) {
			Account entity = new Account(viewModel.getIdUser(),viewModel.getEmail(),viewModel.getPassword(),viewModel.getEnabled(),viewModel.getUsername());
			return entity;
		}

		
		//Mapper UserProfile
		@Autowired
		private UserRepository userRepository;
		
		public UserProfileViewModel convertToUserProfileViewModel(UserProfile entity) {
			UserProfileViewModel viewModel = new UserProfileViewModel();
			viewModel.setIdProfile(entity.getIdProfile());
			viewModel.setIdUser(entity.getAccount().getIdUser());
			viewModel.setNameUser(entity.getNameUser());
			viewModel.setNumberDocument(entity.getNumberDocument());
			viewModel.setTypeDocument(entity.getTypeDocument());
			return viewModel;
		}
		
		public UserProfile convertToUserProfileEntity(UserProfileViewModel viewModel) {
			Account user = this.userRepository.findById(viewModel.getIdUser()).get();
			UserProfile entity = new UserProfile(viewModel.getIdProfile(),viewModel.getTypeDocument(),viewModel.getNumberDocument(),viewModel.getNameUser(),user);
			return entity;
		}
}
