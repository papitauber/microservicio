package com.grupoWeb.api.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;
import javax.validation.ValidationException;

import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.grupoWeb.Mapper;
import com.grupoWeb.api.viewmodel.UserViewModel;
import com.grupoWeb.model.entity.Account;
import com.grupoWeb.service.UserService;


@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService userService;
	
	
	@Autowired
	private Mapper mapper;
	
	@GetMapping
	public List<UserViewModel> all(){
		List<Account> users = this.userService.getAll();
		List<UserViewModel> userViewModel = users.stream().map(user -> this.mapper.convertToUserViewModel(user))
				.collect(Collectors.toList());
		return userViewModel;
	}
	
	@GetMapping("/{id}")
	public UserViewModel byId(@PathVariable Long id) {
		Optional<Account> user = this.userService.getOne(id);
		if(!user.isPresent()) {
			throw new EntityNotFoundException();
		}
		UserViewModel userViewModel = this.mapper.convertToUserViewModel(user.get()); 
		return userViewModel;
	}
	
	@PostMapping
	public Account save(@RequestBody UserViewModel userViewModel,BindingResult bindingResult) {
		if(bindingResult.hasErrors()) {
			throw new ValidationException();
		}
		Account userEntity = this.mapper.convertToUserEntity(userViewModel);
		this.userService.insertOrUpdate(userEntity);
		return userEntity;
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id) {
		this.userService.delete(id);
	}
	@PostMapping("/login")
	public Account login(@RequestBody UserViewModel userViewModel, BindingResult bindingResult) {
		if(bindingResult.hasErrors()) {
			throw new ValidationException();
		}else {
			if(this.userService.getUsername(userViewModel.getUsername())!=null && this.userService.getPassword(userViewModel.getPassword())!=null) {
				Account userEntity = this.mapper.convertToUserEntity(userViewModel);
				userEntity.setEnabled(true);
				userEntity.setIdUser(this.userService.getIdByUserName(userViewModel.getUsername()));
				return userEntity;
			}
			else {
				return null;
			}
		}
	}
}
