package com.grupoWeb.api.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;
import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.grupoWeb.Mapper;
import com.grupoWeb.api.viewmodel.RolViewModel;
import com.grupoWeb.model.entity.Rol;
import com.grupoWeb.service.RolService;


@RestController
@RequestMapping("/rol")
public class RolController {

	@Autowired
	private RolService rolService;
	
	@Autowired
	private Mapper mapper;
	
	@GetMapping
	public List<RolViewModel> all(){
		List<Rol> rols = this.rolService.getAll();
		List<RolViewModel> rolViewModel = rols.stream().map(rol -> this.mapper.convertToRolViewModel(rol))
				.collect(Collectors.toList());
		return rolViewModel;
	}
	
	@GetMapping("/{id}")
	public RolViewModel byId(@PathVariable Long id) {
		Optional<Rol> rol = this.rolService.getOne(id);
		if(!rol.isPresent()) {
			throw new EntityNotFoundException();
		}
		RolViewModel rolViewModel = this.mapper.convertToRolViewModel(rol.get()); 
		return rolViewModel;
	}
	
	@PostMapping
	public Rol save(@RequestBody RolViewModel rolViewModel,BindingResult bindingResult) {
		if(bindingResult.hasErrors()) {
			throw new ValidationException();
		}
		Rol rolEntity = this.mapper.convertToRolEntity(rolViewModel);
		this.rolService.insertOrUpdate(rolEntity);
		return rolEntity;
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id) {
		this.rolService.delete(id);
	}
}
