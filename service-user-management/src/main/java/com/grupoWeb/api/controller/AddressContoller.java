package com.grupoWeb.api.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;
import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.grupoWeb.Mapper;
import com.grupoWeb.api.viewmodel.AddressViewModel;
import com.grupoWeb.model.entity.Address;
import com.grupoWeb.service.AddressService;
import com.grupoWeb.service.DistrictService;


@RestController
@RequestMapping("/address")
public class AddressContoller {
	@Autowired
	private AddressService addressService;
	
	@Autowired
	private Mapper mapper;
	
	@Autowired
	private DistrictService districtService;
	
	@GetMapping
	public List<AddressViewModel> all(){
		List<Address> addresses = this.addressService.getAll();
		List<AddressViewModel> providerViewModel = addresses.stream().map(address->this.mapper.convertToAddressViewModel(address)).collect(Collectors.toList());
		return providerViewModel;
	}

	@GetMapping("/{id}")
	public AddressViewModel byId(@PathVariable Long id) {
		Optional<Address> address = this.addressService.getOne(id);
		if(!address.isPresent()) {
			throw new EntityNotFoundException();
		}
		AddressViewModel addressViewModel = this.mapper.convertToAddressViewModel(address.get());
		return addressViewModel;
	}
	
	@PostMapping
	public Address save(@RequestBody AddressViewModel addressViewModel, BindingResult bindingResult) {
		if(bindingResult.hasErrors()) {
			throw new ValidationException();
		}
		Address addressEntity = this.mapper.converToAddressEntity(addressViewModel);
		this.addressService.insertOrUpdate(addressEntity);
		return addressEntity;
	}
	
	@PutMapping
	public Address update(@RequestBody AddressViewModel providerViewModel, BindingResult bindingResult) {
		if(bindingResult.hasErrors()) {
			throw new ValidationException();
		}
		Address addressEntity = this.mapper.converToAddressEntity(providerViewModel);
		this.addressService.insertOrUpdate(addressEntity);
		return addressEntity;
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id) {
		this.addressService.delete(id);
	}

	
}
