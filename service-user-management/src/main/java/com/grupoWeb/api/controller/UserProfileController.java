package com.grupoWeb.api.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;
import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.grupoWeb.Mapper;
import com.grupoWeb.api.viewmodel.UserProfileViewModel;
import com.grupoWeb.model.entity.UserProfile;
import com.grupoWeb.service.UserProfileService;

@RestController
@RequestMapping("/user-profile")
public class UserProfileController {
	@Autowired
	private UserProfileService userProfileService;
	
	@Autowired
	private Mapper mapper;
	
	@GetMapping
	public List<UserProfileViewModel> all(){
		List<UserProfile> profiles = this.userProfileService.getAll();
		List<UserProfileViewModel> profileViewModel = profiles.stream().map(profile -> this.mapper.convertToUserProfileViewModel(profile))
				.collect(Collectors.toList());
		return profileViewModel;
	}
	
	@GetMapping("/{id}")
	public UserProfileViewModel byId(@PathVariable Long id) {
		Optional<UserProfile> profile = this.userProfileService.getOne(id);
		if(!profile.isPresent()) {
			throw new EntityNotFoundException();
		}
		UserProfileViewModel profileViewModel = this.mapper.convertToUserProfileViewModel(profile.get()); 
		return profileViewModel;
	}
	
	@PostMapping
	public UserProfile save(@RequestBody UserProfileViewModel profileViewModel,BindingResult bindingResult) {
		if(bindingResult.hasErrors()) {
			throw new ValidationException();
		}
		UserProfile userProfileEntity = this.mapper.convertToUserProfileEntity(profileViewModel);
		this.userProfileService.insertOrUpdate(userProfileEntity);
		return userProfileEntity;
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id) {
		this.userProfileService.delete(id);
	}
}
