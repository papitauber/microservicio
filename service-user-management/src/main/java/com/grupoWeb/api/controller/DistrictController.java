package com.grupoWeb.api.controller;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;
import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.grupoWeb.Mapper;
import com.grupoWeb.api.viewmodel.DistrictViewModel;
import com.grupoWeb.model.entity.District;
import com.grupoWeb.service.DistrictService;


@RestController
@RequestMapping("/district")
public class DistrictController {


	@Autowired
	private DistrictService districtService;
	
	@Autowired
    private Mapper mapper;
	
	@GetMapping
	public List<District> all(){
		List<District> districts = this.districtService.getAll();
		return districts;
	}
	
	@GetMapping("/{id}")
	public DistrictViewModel byId(@PathVariable Long id) {
		Optional<District> district = this.districtService.getOne(id);
		if(!district.isPresent()) {
			throw new EntityNotFoundException();
		}
		DistrictViewModel districtViewModel = this.mapper.convertToDistrictViewModel(district.get());
		return districtViewModel;
	}
	
	@PostMapping
	public District save(@RequestBody DistrictViewModel districtViewModel,BindingResult bindingResult) {
		if(bindingResult.hasErrors()) {
			throw new ValidationException();
		}
		
		District districtEntity=this.mapper.convertToDistrictEntity(districtViewModel);
		this.districtService.insertOrUpdate(districtEntity);
		return districtEntity;
	}
	
	@PutMapping
	public District update(@RequestBody District district,BindingResult bindingResult) {
		if(bindingResult.hasErrors()) {
			throw new ValidationException();
		}
		
		District districtNew = this.districtService.insertOrUpdate(district);
		return districtNew;
	}
	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id) {
		this.districtService.delete(id);
	}
}
