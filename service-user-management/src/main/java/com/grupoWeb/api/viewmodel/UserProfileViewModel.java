package com.grupoWeb.api.viewmodel;

public class UserProfileViewModel {
	private Long idProfile;
	
	// 0: Ninguno, 1:DNI,2:RUC,3:Carnet de Extranjeria
	private Integer typeDocument;
	
	private String numberDocument;
	
	private String nameUser;
	
	private Long idUser;

	public Long getIdProfile() {
		return idProfile;
	}

	public void setIdProfile(Long idProfile) {
		this.idProfile = idProfile;
	}

	public Integer getTypeDocument() {
		return typeDocument;
	}

	public void setTypeDocument(Integer typeDocument) {
		this.typeDocument = typeDocument;
	}

	public String getNumberDocument() {
		return numberDocument;
	}

	public void setNumberDocument(String numberDocument) {
		this.numberDocument = numberDocument;
	}

	public String getNameUser() {
		return nameUser;
	}

	public void setNameUser(String nameUser) {
		this.nameUser = nameUser;
	}

	public Long getIdUser() {
		return idUser;
	}

	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}
	
	
}
