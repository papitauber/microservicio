package com.grupoWeb.api.viewmodel;


public class AddressViewModel {

	private Long idAddress;

	private Integer typeRoad;

	private String nameAddress;

	private Integer numberAddress;

	private String blockNumber;
	
	private Long idDistrict;
	
	private Long idProfile;

	public Long getIdAddress() {
		return idAddress;
	}

	public void setIdAddress(Long idAddress) {
		this.idAddress = idAddress;
	}

	public Integer getTypeRoad() {
		return typeRoad;
	}

	public void setTypeRoad(Integer typeRoad) {
		this.typeRoad = typeRoad;
	}

	public String getNameAddress() {
		return nameAddress;
	}

	public void setNameAddress(String nameAddress) {
		this.nameAddress = nameAddress;
	}

	public Integer getNumberAddress() {
		return numberAddress;
	}

	public void setNumberAddress(Integer numberAddress) {
		this.numberAddress = numberAddress;
	}

	public String getBlockNumber() {
		return blockNumber;
	}

	public void setBlockNumber(String blockNumber) {
		this.blockNumber = blockNumber;
	}

	public Long getIdDistrict() {
		return idDistrict;
	}

	public void setIdDistrict(Long idDistrict) {
		this.idDistrict = idDistrict;
	}

	public Long getIdProfile() {
		return idProfile;
	}

	public void setIdProfile(Long idProfile) {
		this.idProfile = idProfile;
	}

	
}
