package com.grupoWeb.api.viewmodel;

public class DistrictViewModel {
	private Long idDistric;
	private String name;
	public Long getIdDistric() {
		return idDistric;
	}
	public void setIdDistric(Long idDistric) {
		this.idDistric = idDistric;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
